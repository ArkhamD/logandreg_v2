package com.example.logandreg

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.logandreg.databinding.FragmentHomeBinding
import com.example.logandreg.databinding.FragmentRegistrationBinding
import java.util.zip.Inflater

class RegistrationFragment : Fragment() {
    lateinit var binding: FragmentRegistrationBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegistrationBinding.inflate(inflater)

        binding.btnToLogin.setOnClickListener {
            Navigation.
            findNavController(binding.root).
            navigate(R.id.action_registrationFragment_to_loginFragment)
        }
        /*val btn = view.findViewById<Button>(R.id.btnToLogin)
        btn.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment2_to_loginFragment)
        }*/

        return binding.root
    }

}