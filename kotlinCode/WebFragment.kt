package com.example.logandreg

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnKeyListener
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.example.logandreg.databinding.FragmentWebBinding

class WebFragment : Fragment() {

    lateinit var binding : FragmentWebBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWebBinding.inflate(inflater)

        webViewSetup()
        return binding.root
    }

    private fun webViewSetup() {
        binding.webView.webViewClient = WebViewClient()

        binding.webView.apply {
            loadUrl("https://www.google.ru/")
        }
    }

}